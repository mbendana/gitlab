# Answer to question 1, script 1 (used manual pages and bash):
### First Script:
```
#!/bin/bash

#Using awk
awk -F: ' { print $1":"$6 } ' /etc/passwd

#Using sed
#sed 's/\(.*\):x:.*:.*:.*:\(.*\):.*/\1:\2/g' /etc/passwd

#Using cut
#cut -d: -f1,6 /etc/passwd
```

# Answer to question 1, script 2 (used manual pages and bash):
### Second Script:
```
#!/bin/bash
  
log_dir=/var/log
md5_hash_script="$($HOME/bin/users_and_directories.sh | md5sum)"
  
if [[ -e ${log_dir}/current_users ]]
  then
  md5_hash_file="$(cat ${log_dir}/current_users)"

  if [[ $md5_hash_script != $md5_hash_file ]]
  then
    echo "$(date +"%D %T") - Changes occurred from ${md5_hash_file:0:32} to ${md5_hash_script:0:32}" >> ${log_dir}/user_changes
    echo "$md5_hash_script" > ${log_dir}/current_users
  fi
else
  echo "$md5_hash_script" > ${log_dir}/current_users
fi
```
### Crontab:
```
@hourly $HOME/bin/users_and_directories.sh
@hourly $HOME/bin/log_users.sh
```

# Answer to question 2:

The problem could be that all services are running on one machine. Nowadays, microservices are very common for the separation of applications and their purpose. Container technologies, like Docker, and orchestration ones, like Kubernetes, are widely used to help with these type of issues.

# Answer to question 3 (used knowledge from Pro Git book):

On master branch:
first commit and second commit:
`git commit -m <message>`

new feature branch:
`git checkout -b feature-branch`

On feature-branch:
`git commit -m <message>`

Back on master branch:
third commit:
`git commit -m <message>`

Merge:
`git merge feature-branch`

fourth commit:
`git commit -m <message>`

# Answer to question 4 (checked git --help and the corresponding subcommands: branch, switch and checkout):

### Using Git to implement a new feature/change without affecting the main branch

#### Why:
Branches in general are used to deviate from the main branch for different purposes like adding new features, patching or fixing bugs.

#### How:
The `git branch <branchname>` command creates a new branch poiting to the current branch.

If a different branch needs to be specified as the "start" or "head" branch, the `git branch <branchname> [<start-point>]` command can be used.

After the branch is created you will need to switch to it by using the `git switch <branchname>` command.

You could also run the `git checkout -b <branchname>` or `git checkout -b <branchname> [<start-point>]` or the `git switch -c <branchname>` or `git switch -c <branchname> [<start-point>]` commands to create and switch to the new branch using one command.


# Answer to question 5:

Pro Git. Haven't finished it yet. Very detailed.
